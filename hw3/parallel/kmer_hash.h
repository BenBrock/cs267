#ifndef KMER_HASH_H
#define KMER_HASH_H


#ifndef KMER_LENGTH
#define KMER_LENGTH 19
#endif

#ifndef LINE_SIZE
#define LINE_SIZE (KMER_LENGTH+4)
#endif 

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h> 
#include <string.h>
#include "contig_generation.h"

/* Creates a hash table and (pre)allocates memory for the memory heap */
hash_table_t* create_hash_table(int64_t nEntries, memory_heap_t *memory_heap)
{
   hash_table_t *result;
   int64_t n_buckets = nEntries * LOAD_FACTOR;
   int64_t buckets_per_thread = ((n_buckets + THREADS - 1)/THREADS);
   n_buckets = buckets_per_thread * THREADS;

   result = malloc(sizeof(hash_table_t));

   result->size = n_buckets;
   result->table = upc_all_alloc(THREADS, sizeof(bucket_t)*buckets_per_thread);
   upc_barrier;

   // CL: Replacing the calloc with all_alloc + the below
   upc_forall(int i = 0; i < n_buckets; i++; i/buckets_per_thread) {
      result->table[i].head = NULL;
      result->table[i].lock = upc_global_lock_alloc();
   }
   
   int64_t entries_per_thread = (nEntries + THREADS -1)/THREADS;


   memory_heap->heap = upc_all_alloc(THREADS, sizeof(kmer_t)*entries_per_thread);
   upc_barrier;

   // if (memory_heap->heap == NULL) {
   //    fprintf(stderr, "ERROR: Could not allocate memory for the heap!\n");
   //    exit(1);
   // }
   if (MYTHREAD == 0){
      posInHeap = 0;
   }
   //memory_heap->lock = upc_all_lock_alloc();
   upc_barrier;
   return result;
}

/* Auxiliary function for computing hash values */
int64_t hashseq(int64_t  hashtable_size, char *seq, int size)
{
   unsigned long hashval;
   hashval = 5381;
   for(int i = 0; i < size; i++) {
      hashval = seq[i] +  (hashval << 5) + hashval;
   }
   
   return hashval % hashtable_size;
}

/* Returns the hash value of a kmer */
int64_t hashkmer(int64_t  hashtable_size, char *seq)
{
   return hashseq(hashtable_size, seq, KMER_PACKED_LENGTH);
}

/* Looks up a kmer in the hash table and returns a pointer to that entry */
shared kmer_t* lookup_kmer(hash_table_t *hashtable,  memory_heap_t *memory_heap, unsigned char *kmer)
{

   // printf("%i looking up: ", MYTHREAD);
   // for (int i = 0; i <KMER_LENGTH; i++) printf("%c", kmer[i]);
   // printf("\n");

   unsigned char packedKmer[KMER_PACKED_LENGTH];
   packSequencePrivate(kmer, (unsigned char*) packedKmer, KMER_LENGTH);
   // printf("looking up: ");
   // for (int i = 0; i <5; i++) printf("%i ", packedKmer[i]);
   // printf("\n");
 
   int64_t hashval = hashkmer(hashtable->size, (char*) packedKmer);
   shared bucket_t *cur_bucket;
   shared kmer_bucket_t *result;
   cur_bucket = &(hashtable->table[hashval]);
   result = cur_bucket->head;
 //  printf("%i got result of hashval %lli to ptr location %lli\n", MYTHREAD, hashval, result);


   for (; result!=NULL; ) {
      // printf("(%i) hai u not null? \n", MYTHREAD);
      // printf("%i    has kmer: ", MYTHREAD);
      // for (int i = 0; i <KMER_PACKED_LENGTH; i++) printf("%i ", (unsigned char) memory_heap->heap[result->pos].kmer[i]);
      // printf("\n");

      // printf("%i    packed kmer: ", MYTHREAD);
      // for (int i = 0; i <KMER_PACKED_LENGTH; i++) printf("%i ", packedKmer[i]);
      // printf("\n");

   //  printf("%i memcmp of these two: %i\n", MYTHREAD, memcmp(packedKmer, memory_heap->heap[result->pos].kmer, KMER_PACKED_LENGTH * sizeof(char)));
      int i;
      for (i = 0; i < KMER_PACKED_LENGTH; i ++){
         if (((unsigned char)memory_heap->heap[result->pos].kmer[i]) != packedKmer[i]){
          //  printf("(%i )position %i was not equal: %d, %d\n", MYTHREAD, i, (unsigned char) memory_heap->heap[result->pos].kmer[i], packedKmer[i]);
            result = result->next;
            break;
         }
     // printf("(%i )position %i was  equal\n", MYTHREAD, i);
      }
      if (i == KMER_PACKED_LENGTH){
      //   printf("(%i) HALO AM RETURNING, right extension is %c\n", MYTHREAD, memory_heap->heap[result->pos].r_ext);
         return &(memory_heap->heap[result->pos]);         
      }


      // if ( memcmp(packedKmer, result->kmer, KMER_PACKED_LENGTH * sizeof(char)) == 0 ) {
      //    printf("(%i) HALO AM RETURNING, right extension is %c\n", MYTHREAD, result->r_ext);
      //    return result;
      // }
      // result = result->next;
      // printf("%i got next of hashval %lli to ptr location %lli\n", MYTHREAD, hashval, result);
 
   };
   printf("(%i) dayumn that result is null\n", MYTHREAD);
   return NULL;
}

/* Adds a kmer and its extensions in the hash table (note that a memory heap should be preallocated. ) */
int64_t add_kmer(hash_table_t *hashtable, memory_heap_t *memory_heap, shared [LINE_SIZE] unsigned char *kmer, 
   char left_ext, char right_ext)
{
   // // /* Pack a k-mer sequence appropriately */
   char packedKmer[KMER_PACKED_LENGTH];
   packSequence(kmer, (unsigned char*) packedKmer, KMER_LENGTH);
   int64_t hashval = hashkmer(hashtable->size, (char*) packedKmer);

   // printf("%i trying to insert kmer: ", MYTHREAD);
   // for (int i = 0; i <KMER_LENGTH; i++) printf("%c", kmer[i]);
   // printf("\n");

   // printf("%i trying to insert kmer: ", MYTHREAD);
   // for (int i = 0; i <KMER_PACKED_LENGTH; i++) printf("%i ", packedKmer[i]);
   // printf("\n");

   upc_lock(memory_heap->lock);
//printf("(%i) pos b4 increment: %i\n", MYTHREAD, posInHeap);
   int64_t pos = posInHeap++;
   // printf("(%i)  pos %i\n", MYTHREAD,pos);
   // printf("%i heap b4 insert\n", MYTHREAD);
   //    for (int i = 0; i < pos; i++){
   //    //kmer_t tmp= memory_heap[i];
   //    printf("Thread (%i) at pos %i (%lli): ", MYTHREAD, i, &(memory_heap->heap[i]));
   //    for (int j = 0; j < 5; j++) printf("%i ", (unsigned char) memory_heap->heap[i].kmer[j]);
   //    printf("\n");
   // }
   upc_unlock(memory_heap->lock);

   // /* Add the contents to the appropriate kmer struct in the heap */
   // memcpy((memory_heap->heap[pos]).kmer, packedKmer, KMER_PACKED_LENGTH * sizeof(char));
   upc_memput((memory_heap->heap[pos]).kmer, packedKmer, KMER_PACKED_LENGTH * sizeof(char));
   memory_heap->heap[pos].l_ext = left_ext;
   memory_heap->heap[pos].r_ext = right_ext;

   upc_lock(hashtable->table[hashval].lock);
   //  Fix the next pointer to point to the appropriate kmer struct 
 //  (memory_heap->heap[pos]).next = hashtable->table[hashval].head;
   // printf("%i assigned next of hashval %lli to ptr location %lli\n", MYTHREAD, hashval,(memory_heap->heap[pos]).next);
   shared kmer_bucket_t * new_head = upc_global_alloc(1,sizeof(kmer_bucket_t));
   new_head->pos = pos;
   new_head->next = hashtable->table[hashval].head;
   // /* Fix the head pointer of the appropriate bucket to point to the current kmer */
   hashtable->table[hashval].head = new_head;//&(memory_heap->heap[pos]);
   upc_unlock(hashtable->table[hashval].lock);
   // upc_lock(memory_heap->lock);

   //    printf("%i heap after insert\n", MYTHREAD);
   //    for (int i = 0; i < pos+1; i++){
   //    //kmer_t tmp= memory_heap[i];
   //    printf("Thread (%i) at pos %i (%lli): ", MYTHREAD, i, &(memory_heap->heap[i]));
   //    for (int j = 0; j < 5; j++) printf("%i ", (unsigned char) memory_heap->heap[i].kmer[j]);
   //    printf("\n");
   //    }
   //    upc_unlock(memory_heap->lock);

   return pos;
}

/* Adds a k-mer in the start list by using the memory heap (the k-mer was "just added" in the memory heap at position pos */
// TODO: type of startKmersList doesn't seem quite right.
//       This should be a shared pointer to shared pointers to kmer_t's,
//       not a shared pointer to private pointers to kmer_t's.
void addKmerToStartList(memory_heap_t *memory_heap, start_kmer_t ** startKmersList, int64_t pos)
{
   start_kmer_t *new_entry;

   new_entry = (start_kmer_t*) malloc(sizeof(start_kmer_t));
   new_entry->next = (*startKmersList);
   new_entry->kmer_pos = pos;
   (*startKmersList) = new_entry;
}

/* Deallocation functions */
int dealloc_heap(memory_heap_t *memory_heap)
{
   upc_free(memory_heap->heap);
   return 0;
}

int dealloc_hashtable(hash_table_t *hashtable)
{
   upc_free(hashtable->table);
   return 0;
}


#endif // KMER_HASH_H
