#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <time.h>
#include <upc.h>
#include <upc_io.h>

#include "packingDNAseq.h"
#include "kmer_hash.h"

// global because lifetimes :|
shared int64_t GnKmers;
memory_heap_t memory_heap;


int main(int argc, char *argv[]){

	/** Declarations **/
	double inputTime=0.0, constrTime=0.0, traversalTime=0.0;

	char left_ext, right_ext, * input_UFX_name;

	upc_lock_t * nKmer_lock = upc_all_lock_alloc(), *startList_lock = upc_all_lock_alloc();
	upc_barrier;
	

	int64_t total_chars_to_read, cur_chars_read;
    // buffer to read into
	upc_file_t *inputFile; // TODO: outfile
	/* Read the input file name */
	input_UFX_name = argv[1];


	/** Read input **/
	upc_barrier;
	inputTime -= gettime();
    ///// START READ ///////////////////////////////////////////
	init_LookupTable(); // should the lookup table be shared?
	// TODO: this is excessive, only one thread needs to compute nKmers
	if (MYTHREAD == 0) {
		GnKmers = getNumKmersInUFX(input_UFX_name);
	}
	upc_barrier;
	int64_t nKmers = GnKmers;

	total_chars_to_read = nKmers * LINE_SIZE;
	// yes, the last processor will have a smaller chunk. 
	int64_t chars_per_thread = (total_chars_to_read + THREADS -1 )/THREADS;
	int64_t lines_per_thread = (nKmers+THREADS-1) / THREADS;


	shared [LINE_SIZE] unsigned char *working_buffer = upc_all_alloc(THREADS, lines_per_thread*LINE_SIZE);
	// UPC_COMMON_FP vs. UPC_INDIVIDUAL_FP
	inputFile = upc_all_fopen(input_UFX_name, UPC_RDONLY | UPC_COMMON_FP, 0, NULL);
	// CL: w.h.p some math is wrong (BB: Looks good to me, although added upc_flag_t flags just in case.)
	cur_chars_read = upc_all_fread_shared(inputFile, working_buffer, LINE_SIZE, sizeof(unsigned char), lines_per_thread*THREADS*LINE_SIZE, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
	//cur_chars_read = fread(working_buffer, sizeof(unsigned char),total_chars_to_read , inputFile);
	upc_all_fclose(inputFile);
    /////  END READ  ///////////////////////////////////////////
	upc_barrier;
	inputTime += gettime();

	/** Graph construction **/
	constrTime -= gettime();

	hash_table_t * hashtable = malloc(sizeof(hash_table_t));
    hashtable->lock = upc_all_lock_alloc();
	memory_heap.lock = upc_all_lock_alloc();

	upc_barrier;

	hashtable = create_hash_table(nKmers, &memory_heap); 

	int lineBegin = (MYTHREAD*chars_per_thread) / LINE_SIZE;
	int size = chars_per_thread / LINE_SIZE;

	printf("%d gets %d -> %d\n", MYTHREAD, lineBegin, lineBegin+size);
	start_kmer_t *startKmersList = NULL;
	upc_forall(int li = 0; li < nKmers; li++; li / size) {
		int i = li * LINE_SIZE;
		left_ext = (char) working_buffer[i+KMER_LENGTH+1];
		right_ext = (char) working_buffer[i+KMER_LENGTH+2];
		int64_t heap_pos = add_kmer(hashtable, &memory_heap, &working_buffer[i], left_ext, right_ext);

		if (left_ext == 'F'){
		  	upc_lock(startList_lock);
		 	addKmerToStartList(&memory_heap, &startKmersList, heap_pos);
		 	upc_unlock(startList_lock);
		}
	}

	upc_barrier;
	constrTime += gettime();

	// if (MYTHREAD == 0)
	// for (int i = 0; i < nKmers; i++){
	// 	//kmer_t tmp= memory_heap[i];
	// 	printf("Thread (%i) at pos %i (%lli): ", MYTHREAD, i, &(memory_heap.heap[i]));
	// 	for (int j = 0; j < 5; j++) printf("%i ", (unsigned char) memory_heap.heap[i].kmer[j]);
	// 	printf(", lext: %c, rext:%c\n",memory_heap.heap[i].l_ext, memory_heap.heap[i].r_ext);
	// }
	// upc_barrier;

	/** Graph traversal **/
	traversalTime -= gettime();
	char buffer[32];
	sprintf(buffer, "pgen_%d.out", MYTHREAD);
	FILE *serialOutputFile = fopen(buffer, "w");

	printf("%d writing to %s\n", MYTHREAD, buffer);

	char unpackedKmer[KMER_LENGTH+1];
	char cur_contig[MAXIMUM_CONTIG_SIZE];
	unpackedKmer[KMER_LENGTH] = '\0';
	int64_t posInContig;
	int64_t contigID = 0;
	int64_t totBases = 0;

	int count = 0;
	for (start_kmer_t *ki = startKmersList; ki != NULL; ki = ki->next) {
	//	printf("Thread %i at count %i\n", MYTHREAD, count);
		start_kmer_t *curStartNode = ki;
		shared kmer_t *cur_kmer_ptr = &memory_heap.heap[curStartNode->kmer_pos];
		char packedKmer[KMER_PACKED_LENGTH];
		upc_memget(packedKmer, memory_heap.heap[curStartNode->kmer_pos].kmer, KMER_PACKED_LENGTH);
		unpackSequence(packedKmer,  (unsigned char*) unpackedKmer, KMER_LENGTH);
		memcpy(cur_contig, unpackedKmer, KMER_LENGTH * sizeof(char));
		posInContig = KMER_LENGTH;
		right_ext = cur_kmer_ptr->r_ext;
		int tmp =0;
		while (right_ext != 'F') {

			//printf("(%i): tmp %i\n", MYTHREAD, tmp);
			cur_contig[posInContig] = right_ext;
			posInContig++;
						// printf("Contig so far: %.*s\n", posInContig, cur_contig);

			cur_kmer_ptr = lookup_kmer(hashtable, &memory_heap,&cur_contig[posInContig-KMER_LENGTH]);
			//printf("cur kmer ptr: %lli\n", cur_kmer_ptr);
			right_ext = cur_kmer_ptr->r_ext;
		//	printf("(%i): right_extension %c\n", MYTHREAD, right_ext);
			tmp++;
			   if (right_ext == '\0'){
      			printf("2(%i) Null right extension :(\n", MYTHREAD);
      			exit(0);

   }
			
		}
		
		cur_contig[posInContig] = '\0';
		fprintf(serialOutputFile,"%s\n", cur_contig);
		contigID++;
		totBases += strlen(cur_contig);


		count++;
	}
	fclose(serialOutputFile);
	printf("(%d) Processed %d Kmers.\n", MYTHREAD, count);
	upc_barrier;
	traversalTime += gettime();

	/** Print timing and output info **/
	/***** DO NOT CHANGE THIS PART ****/
	if(MYTHREAD==0){
		printf("%s: Input set: %s\n", argv[0], argv[1]);
		printf("Number of UPC threads: %d\n", THREADS);
		printf("Input reading time: %f seconds\n", inputTime);
		printf("Graph construction time: %f seconds\n", constrTime);
		printf("Graph traversal time: %f seconds\n", traversalTime);
	}

	return 0;
}
