#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <vector>
#include <list>
#include <tuple>
#include "common.h"
#include "omp.h"
#include <atomic>

template <class T>
class AtomicArray {
public:
    int capacity;
    std::atomic <int> *asize;

    std::vector <T> data;

    AtomicArray(int size) {
        this->capacity = size;

        data.resize(this->capacity);
        this->asize = new std::atomic <int> (0);
    }

    ~AtomicArray() {
        delete this->asize;
    }

    void push_back(T val) {
        int old_size = (*asize)++;
        if (old_size >= this->capacity) {
            printf("ERROR! Overfilling AtomicArray.\n");
            exit(0);
        }
        data[old_size] = val;
    }

    T get(int i) {
        if (i < 0 || i >= this->capacity) {
            printf("ERROR! Accessing out of bounds element %d in AtomicArray.\n", i);
            exit(0);
        }
        return data[i];
    }

    void reset() {
        *(this->asize) = 0;
    }

    size_t size() {
        return this->asize->load();
    }
};

//
//  benchmarking program
//
int main( int argc, char **argv )
{   
    int navg,nabsavg=0,numthreads; 
    double dmin, absmin=1.0,davg,absavg=0.0;
	
    if( find_option( argc, argv, "-h" ) >= 0 )
    {
        printf( "Options:\n" );
        printf( "-h to see this help\n" );
        printf( "-n <int> to set number of particles\n" );
        printf( "-o <filename> to specify the output file name\n" );
        printf( "-s <filename> to specify a summary file name\n" ); 
        printf( "-no turns off all correctness checks and particle output\n");   
        return 0;
    }

    int n = read_int( argc, argv, "-n", 1000 );
    char *savename = read_string( argc, argv, "-o", NULL );
    char *sumname = read_string( argc, argv, "-s", NULL );

    FILE *fsave = savename ? fopen( savename, "w" ) : NULL;
    FILE *fsum = sumname ? fopen ( sumname, "a" ) : NULL;      


    particle_t *particles = (particle_t*) malloc( n * sizeof(particle_t) );
    double size = set_size(n);
    double cutoff = get_cutoff();
    init_particles( n, particles );

    double bin_size = cutoff;

    int bindim = ((int) (ceil(size / bin_size) + 0.5));
    int nbins = bindim*bindim;

    // printf("grid dimensions are %d -> %lf and cutoff is %lf, so %d bins of size %lf\n", 0, size, cutoff, nbins, bin_size);

    //
    //  simulate a number of time steps
    //
    double simulation_time = read_timer();

    int bin_capacity = 32;
    std::vector <AtomicArray <int> *> bins (nbins, NULL);


    #pragma omp parallel private(dmin) shared(bins)
    {
    numthreads = omp_get_num_threads();

    #pragma omp for
    for (int i = 0; i < nbins; i++) {
        bins[i] = new AtomicArray <int> (bin_capacity);
    }

    for( int step = 0; step < NSTEPS; step++ )
    {
        navg = 0;
        davg = 0.0;
	    dmin = 1.0;

        #pragma omp for
        for (int i = 0; i < nbins; i++) {
            bins[i]->reset();
        }

        #pragma omp barrier

        // Bin the particles!
        #pragma omp for
        for (int i = 0; i < n; i++) {
            int xbin = (int) (particles[i].x / cutoff);
            int ybin = (int) (particles[i].y / cutoff);

            bins[xbin + ybin*bindim]->push_back(i);
        }

        #pragma omp barrier

        //
        //  compute all forces
        //
/*
        #pragma omp for reduction (+:navg) reduction(+:davg)
        for( int i = 0; i < n; i++ )
        {
            particles[i].ax = particles[i].ay = 0;
            for (int j = 0; j < n; j++ )
                apply_force( particles[i], particles[j],&dmin,&davg,&navg);
        }
*/

        #pragma omp for reduction (+:navg) reduction(+:davg)
        for (int i = 0; i < bindim; i++) {
            for (int j = 0; j < bindim; j++) {
                for (int p1It = 0; p1It < bins[j + i*bindim]->size(); p1It++) {
                    int p1 = bins[j + i*bindim]->get(p1It);
                    particles[p1].ax = particles[p1].ay = 0;
                    for (int ii = i-1; ii <= i+1; ii++) {
                        for (int jj = j-1; jj <= j+1; jj++) {
                            if (ii >= 0 && ii < bindim && jj >= 0 && jj < bindim) {
                                for (int p2It = 0; p2It < bins[jj + ii*bindim]->size(); p2It++) {
                                    int p2 = bins[jj + ii*bindim]->get(p2It);
                                    apply_force(particles[p1], particles[p2], &dmin,&davg,&navg);
                                }
                            }
                        }
                    }
                }
            }
        }

        #pragma omp barrier

        //
        //  move particles
        //
        #pragma omp for
        for (int i = 0; i < n; i++) {
            move(particles[i]);
        }

        if( find_option( argc, argv, "-no" ) == -1 ) 
        {
          //
          //  compute statistical data
          //
          #pragma omp master
          if (navg) { 
            absavg += davg/navg;
            nabsavg++;
          }

          #pragma omp critical
	  if (dmin < absmin) absmin = dmin; 
		
          //
          //  save if necessary
          //
          #pragma omp master
          if( fsave && (step%SAVEFREQ) == 0 )
              save( fsave, n, particles );
        }
    }
}
    simulation_time = read_timer( ) - simulation_time;
    
    printf( "n = %d,threads = %d, simulation time = %g seconds", n,numthreads, simulation_time);

    if( find_option( argc, argv, "-no" ) == -1 )
    {
      if (nabsavg) absavg /= nabsavg;
    // 
    //  -The minimum distance absmin between 2 particles during the run of the simulation
    //  -A Correct simulation will have particles stay at greater than 0.4 (of cutoff) with typical values between .7-.8
    //  -A simulation where particles don't interact correctly will be less than 0.4 (of cutoff) with typical values between .01-.05
    //
    //  -The average distance absavg is ~.95 when most particles are interacting correctly and ~.66 when no particles are interacting
    //
    printf( ", absmin = %lf, absavg = %lf", absmin, absavg);
    if (absmin < 0.4) printf ("\nThe minimum distance is below 0.4 meaning that some particle is not interacting");
    if (absavg < 0.8) printf ("\nThe average distance is below 0.8 meaning that most particles are not interacting");
    }
    printf("\n");
    
    //
    // Printing summary data
    //
    if( fsum)
        fprintf(fsum,"%d %d %g\n",n,numthreads,simulation_time);

    //
    // Clearing space
    //
    if( fsum )
        fclose( fsum );

    free( particles );
    if( fsave )
        fclose( fsave );
    
    return 0;
}
