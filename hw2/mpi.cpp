#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "common.h"
#include <algorithm>
#include <cmath>
#include <vector>
#include <tuple>
#include <list>

//
//  benchmarking program
//
int main( int argc, char **argv )
{    
    int navg, nabsavg=0;
    double dmin, absmin=1.0,davg,absavg=0.0;
    double rdavg,rdmin;
    int rnavg; 
 
    //
    //  process command line parameters
    //
    if( find_option( argc, argv, "-h" ) >= 0 )
    {
        printf( "Options:\n" );
        printf( "-h to see this help\n" );
        printf( "-n <int> to set the number of particles\n" );
        printf( "-o <filename> to specify the output file name\n" );
        printf( "-s <filename> to specify a summary file name\n" );
        printf( "-no turns off all correctness checks and particle output\n");
        return 0;
    }
    
    int n = read_int( argc, argv, "-n", 1000 );
    char *savename = read_string( argc, argv, "-o", NULL );
    char *sumname = read_string( argc, argv, "-s", NULL );
    
    //
    //  set up MPI
    //
    int n_proc, rank;
    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &n_proc );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    
    //
    //  allocate generic resources
    //
    FILE *fsave = savename && rank == 0 ? fopen( savename, "w" ) : NULL;
    FILE *fsum = sumname && rank == 0 ? fopen ( sumname, "a" ) : NULL;

    particle_t *particles = (particle_t*) malloc( n * sizeof(particle_t) );
    
    MPI_Datatype PARTICLE;
    MPI_Type_contiguous( 6, MPI_DOUBLE, &PARTICLE );
    MPI_Type_commit( &PARTICLE );
    
    //
    //  set up the data partitioning across processors
    //
    
    //
    //  allocate storage for local partition
    //
    
    //
    //  initialize and distribute the particles (that's fine to leave it unoptimized)
    //
    double size = set_size(n);
    double cutoff = get_cutoff();
    double bin_size = cutoff;

    int bindim = ((int) (ceil(size / bin_size) + 0.5));

    int nbins = bindim*bindim;

    int bins_per_rank = ((int) (ceil(((double) bindim) / n_proc) + 0.5));
    int bin_start = rank * bins_per_rank;
    int my_nbins = std::min(bins_per_rank, bindim - bin_start);

    // Processor falls of the end of the universe.
    if (bin_start >= bindim) {
        my_nbins = 0;
        bin_start = 0;
    }

    n_proc = std::min(n_proc, ((int) (ceil(((double) bindim) / bins_per_rank) + 0.5)));

    if (rank == 0)
        init_particles( n, particles );

    MPI_Bcast(particles, n, PARTICLE, 0, MPI_COMM_WORLD);

    std::vector <std::list <particle_t>> local_bins;

    for (int i = 0; i < my_nbins*bindim; i++) {
        local_bins.push_back(std::list <particle_t> ());
    }

    // Bin local particles.
    for (int i = 0; i < n; i++) {
        int xbin = (int) (particles[i].x / bin_size);
        int ybin = (int) (particles[i].y / bin_size);

        if (ybin >= bin_start && ybin < bin_start+my_nbins) {
            int lybin = ybin - bin_start;
            local_bins[xbin + lybin*bindim].push_back(particles[i]);
        }
    }

    //
    //  simulate a number of time steps
    //
    double simulation_time = read_timer( );
    for( int step = 0; step < NSTEPS; step++ )
    {
        navg = 0;
        dmin = 1.0;
        davg = 0.0;

        if (my_nbins > 0) {

        std::vector <particle_t> sendAbove, sendBelow;
        int aboveRank = rank-1;
        int belowRank = rank+1;

        MPI_Request aboveSendRequest;
        MPI_Request belowSendRequest;
        MPI_Request aboveRecvRequest;
        MPI_Request belowRecvRequest;

        // Send number of foreign particles to be transmitted.
        int n_above_recv;
        int n_below_recv;

        if (belowRank < n_proc) {
            MPI_Irecv(&n_below_recv, 1, MPI_INT, belowRank, 0, MPI_COMM_WORLD, &belowRecvRequest);
        }
        if (aboveRank >= 0) {
            MPI_Irecv(&n_above_recv, 1, MPI_INT, aboveRank, 0, MPI_COMM_WORLD, &aboveRecvRequest);
        }

        // Collect particles to send above.
        int n_above = 0;
        for (int i = 0; i < bindim; i++) {
            n_above += local_bins[i].size();
            for (auto j : local_bins[i]) {
                sendAbove.push_back(j);
            }
        }

        if (aboveRank >= 0) {
            MPI_Isend(&n_above, 1, MPI_INT, aboveRank, 0, MPI_COMM_WORLD, &aboveSendRequest);
        }
        if (aboveRank >= 0) {
            MPI_Isend(sendAbove.data(), n_above, PARTICLE, aboveRank, 1, MPI_COMM_WORLD, &aboveSendRequest);
        }

        if (belowRank < n_proc) {
            MPI_Wait(&belowRecvRequest, MPI_STATUS_IGNORE);
        } else {
            n_below_recv = 0;
        }
        particle_t *recv_below = (particle_t *) malloc(n_below_recv * sizeof(particle_t));
        if (belowRank < n_proc) {
            MPI_Irecv(recv_below, n_below_recv, PARTICLE, belowRank, 1, MPI_COMM_WORLD, &belowRecvRequest);
        }

        // Collect particles to send below.
        int n_below = 0;
        for (int i = 0; i < bindim; i++) {
            n_below += local_bins[i + (my_nbins-1)*bindim].size();
            for (auto j : local_bins[i + (my_nbins-1)*bindim]) {
                sendBelow.push_back(j);
            }
        }

        if (belowRank < n_proc) {
            MPI_Isend(&n_below, 1, MPI_INT, belowRank, 0, MPI_COMM_WORLD, &belowSendRequest);
        }
        if (belowRank < n_proc) {
            MPI_Isend(sendBelow.data(), n_below, PARTICLE, belowRank, 1, MPI_COMM_WORLD, &belowSendRequest);
        }

        if (aboveRank >= 0) {
            MPI_Wait(&aboveRecvRequest, MPI_STATUS_IGNORE);
        } else {
            n_above_recv = 0;
        }

        particle_t *recv_above = (particle_t *) malloc(n_above_recv * sizeof(particle_t));

        if (aboveRank >= 0) {
            MPI_Irecv(recv_above, n_above_recv, PARTICLE, aboveRank, 1, MPI_COMM_WORLD, &aboveRecvRequest);
        }

        std::vector <std::list <particle_t>> recvAbove;
        std::vector <std::list <particle_t>> recvBelow;

        for (int i = 0; i < bindim; i++) {
            recvAbove.push_back(std::list <particle_t> ());
            recvBelow.push_back(std::list <particle_t> ());
        }

        // Compute local forces
        for (int i = 0; i < my_nbins; i++) {
            for (int j = 0; j < bindim; j++) {
                for (auto &p1 : local_bins[j + i*bindim]) {
                    p1.ax = p1.ay = 0.0;
                    for (int ii = i-1; ii <= i+1; ii++) {
                        for (int jj = j-1; jj <= j+1; jj++) {
                            if ((ii >= 0 && ii < my_nbins) &&
                                (jj >= 0 && jj < bindim)) {
                                for (auto &p2 : local_bins[jj + ii*bindim]) {
                                    apply_force(p1, p2, &dmin, &davg, &navg);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (belowRank < n_proc) {
            MPI_Wait(&belowRecvRequest, MPI_STATUS_IGNORE);
        }

        // Bin border particles
        for (int i = 0; i < n_below_recv; i++) {
            int xbin = (int) (recv_below[i].x / bin_size);
            recvBelow[xbin].push_back(recv_below[i]);
        }

        free(recv_below);

        // Compute forces from foreign particles below you
        if (belowRank < n_proc) {
            for (int i = 0; i < bindim; i++) {
                for (auto &p1 : local_bins[i + (my_nbins-1)*bindim]) {
                    for (int ii = i-1; ii <= i+1; ii++) {
                        if (ii >= 0 && ii < bindim) {
                            for (auto &p2 : recvBelow[ii]) {
                                apply_force(p1, p2, &dmin, &davg, &navg);
                            }
                        }
                    }
                }
            }
        }

        if (aboveRank >= 0) {
            MPI_Wait(&aboveRecvRequest, MPI_STATUS_IGNORE);
        }

        for (int i = 0; i < n_above_recv; i++) {
            int xbin = (int) (recv_above[i].x / bin_size);
            recvAbove[xbin].push_back(recv_above[i]);
        }
        free(recv_above);

        // Compute forces from foreign particles above you
        if (aboveRank >= 0) {
            for (int i = 0; i < bindim; i++) {
                for (auto &p1 : local_bins[i]) {
                    for (int ii = i-1; ii <= i+1; ii++) {
                        if (ii >= 0 && ii < bindim) {
                            for (auto &p2 : recvAbove[ii]) {
                                apply_force(p1, p2, &dmin, &davg, &navg);
                            }
                        }
                    }
                }
            }
        }
        // Move particles, select particles to rebin
        std::list <std::tuple <std::list <particle_t>::iterator, std::list <particle_t> *>> toRebin;
        for (int i = 0; i < my_nbins; i++) {
            for (int j = 0; j < bindim; j++) {
                for (auto p = local_bins[j + i*bindim].begin(); p != local_bins[j + i*bindim].end(); p++) {
                    double ox = (*p).x;
                    double oy = (*p).y;
                    double ovx = (*p).vx;
                    double ovy = (*p).vy;
                    double oax = (*p).ax;
                    double oay = (*p).ay;
                    move(*p);
                    int xbin = (int) ((*p).x / bin_size);
                    int ybin = (int) ((*p).y / bin_size);
                    int lybin = ybin - bin_start;
                    if (xbin != j || lybin != i) {
                        // Sanity checking.  This should NEVER EVAR HAPPEN (pls *fingers crossed*).
                        if (std::abs(i - lybin) > my_nbins) {
                            double nx = (*p).x;
                            double ny = (*p).y;
                            double nvx = (*p).vx;
                            double nvy = (*p).vy;
                            printf("abs(%d - %d) > %d\n", i, lybin, my_nbins);
                            printf("Error! My particle moved from (%d, %d [%d]) -> (%d, %d [%d])\nThis is (%lf, %lf) -> (%lf, %lf) at velocity (%lf, %lf) -> (%lf, %lf) accel (%lf, %lf)\n", j, i + bin_start, i, xbin, ybin, lybin, ox, oy, nx, ny, ovx, ovy, nvx, nvy, oax, oay);
                            printf("Universe is %lf x %lf\n", size, size);
                            exit(0);
                        }
                        toRebin.push_back(std::make_pair(p, &local_bins[j + i*bindim]));
                    }
                }
            }
        }

        // Rebin particles, select particles to send
        std::vector <particle_t> toSendAbove;
        std::vector <particle_t> toSendBelow;
        for (auto p : toRebin) {
            std::list <particle_t>::iterator pIt = std::get <0> (p);
            std::list <particle_t> *oldBin = std::get <1> (p);
            int xbin = (int) ((*pIt).x / bin_size);
            int ybin = (int) ((*pIt).y / bin_size);
            int lybin = ybin - bin_start;
            if (lybin >= 0 && lybin < my_nbins) {
                local_bins[xbin + lybin*bindim].push_back(*pIt);
            } else {
                if (lybin < 0) {
                    toSendAbove.push_back(*pIt);
                } else {
                    toSendBelow.push_back(*pIt);
                }
            }
            oldBin->erase(pIt);
        }

        int new_above = toSendAbove.size();
        int new_below = toSendBelow.size();

        int new_above_recv;
        int new_below_recv;

        // Send particles which have crossed processor domain boundaries.
        if (belowRank < n_proc) {
            MPI_Irecv(&new_below_recv, 1, MPI_INT, belowRank, 0, MPI_COMM_WORLD, &belowRecvRequest);
        }
        if (aboveRank >= 0) {
            MPI_Irecv(&new_above_recv, 1, MPI_INT, aboveRank, 0, MPI_COMM_WORLD, &aboveRecvRequest);
        }

        if (aboveRank >= 0) {
            MPI_Isend(&new_above, 1, MPI_INT, aboveRank, 0, MPI_COMM_WORLD, &aboveSendRequest);
        }
        if (belowRank < n_proc) {
            MPI_Isend(&new_below, 1, MPI_INT, belowRank, 0, MPI_COMM_WORLD, &belowSendRequest);
        }
        if (aboveRank >= 0) {
            MPI_Isend(toSendAbove.data(), new_above, PARTICLE, aboveRank, 1, MPI_COMM_WORLD, &aboveSendRequest);
        }
        if (belowRank < n_proc) {
            MPI_Isend(toSendBelow.data(), new_below, PARTICLE, belowRank, 1, MPI_COMM_WORLD, &belowSendRequest);
        }

        if (belowRank < n_proc) {
            MPI_Wait(&belowRecvRequest, MPI_STATUS_IGNORE);
        } else {
            new_below_recv = 0;
        }
        particle_t *newParticlesBelow = (particle_t *) malloc(sizeof(particle_t) * new_below_recv);
        if (belowRank < n_proc) {
            MPI_Irecv(newParticlesBelow, new_below_recv, PARTICLE, belowRank, 1, MPI_COMM_WORLD, &belowRecvRequest);
        }

        if (aboveRank >= 0) {
            MPI_Wait(&aboveRecvRequest, MPI_STATUS_IGNORE);
        } else {
            new_above_recv = 0;
        }
        particle_t *newParticlesAbove = (particle_t *) malloc(sizeof(particle_t) * new_above_recv);
        if (aboveRank >= 0) {
            MPI_Irecv(newParticlesAbove, new_above_recv, PARTICLE, aboveRank, 1, MPI_COMM_WORLD, &aboveRecvRequest);
        }

        if (belowRank < n_proc) {
            MPI_Wait(&belowRecvRequest, MPI_STATUS_IGNORE);
        }

        // Bin processor's new particles.
        for (int i = 0; i < new_below_recv; i++) {
            int xbin = (int) (newParticlesBelow[i].x / bin_size);
            int ybin = (int) (newParticlesBelow[i].y / bin_size);
            int lybin = ybin - bin_start;
            if (lybin < 0 || lybin >= my_nbins) {
                printf("ERROR! ERROR! You (%d) sent me (%d) a particle that doesn't belong to me. It belongs in %d, %d (%d)\n", aboveRank, rank, xbin, ybin, lybin);
                exit(0);
            } else {
              local_bins[xbin + lybin*bindim].push_back(newParticlesBelow[i]);
            }
        }

        if (aboveRank >= 0) {
            MPI_Wait(&aboveRecvRequest, MPI_STATUS_IGNORE);
        }

        for (int i = 0; i < new_above_recv; i++) {
            int xbin = (int) (newParticlesAbove[i].x / bin_size);
            int ybin = (int) (newParticlesAbove[i].y / bin_size);
            int lybin = ybin - bin_start;
            if (lybin < 0 || lybin >= my_nbins) {
                printf("ERROR! ERROR! You (%d) sent me (%d) a particle that doesn't belong to me. It belongs in %d, %d (%d)\n", aboveRank, rank, xbin, ybin, lybin);
                exit(0);
            } else {
              local_bins[xbin + lybin*bindim].push_back(newParticlesAbove[i]);
            }
        }

        free(newParticlesAbove);
        free(newParticlesBelow);

        // 
        //  collect all global data locally (not good idea to do)
        //
        // MPI_Allgatherv( local, nlocal, PARTICLE, particles, partition_sizes, partition_offsets, PARTICLE, MPI_COMM_WORLD );
        
        //
        //  save current step if necessary (slightly different semantics than in other codes)
        //
        }
        if (find_option( argc, argv, "-no" ) == -1 ) {
          std::vector <particle_t> local_particles;
          for (int i = 0; i < local_bins.size(); i++) {
            for (auto p : local_bins[i]) {
              local_particles.push_back(p);
            }
          }

          int local_count = local_particles.size();
          if (rank != 0) {
            MPI_Send(&local_count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
          }
          if (rank == 0) {
            std::vector <int> counts;
            counts.push_back(local_count);
            for (int i = 1; i < n_proc; i++) {
              int count;
              MPI_Recv(&count, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
              counts.push_back(count);
            }
            std::vector <int> offsets (counts.size(), 0);
            for (int i = 0; i < counts.size(); i++) {
              if (i+1 < counts.size()) {
                offsets[i+1] = offsets[i] + counts[i];
              }
            }

            MPI_Gatherv(local_particles.data(), local_particles.size(), PARTICLE, particles, counts.data(), offsets.data(), PARTICLE, 0, MPI_COMM_WORLD);
          }

          if (rank != 0) {
            MPI_Gatherv(local_particles.data(), local_particles.size(), PARTICLE, NULL, NULL, NULL, PARTICLE, 0, MPI_COMM_WORLD);
          }

          if( fsave && (step%SAVEFREQ) == 0 )
            save( fsave, n, particles );
        }
        
        //
        //  compute all forces
        //
     
        if( find_option( argc, argv, "-no" ) == -1 )
        {
          
          MPI_Reduce(&davg,&rdavg,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
          MPI_Reduce(&navg,&rnavg,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
          MPI_Reduce(&dmin,&rdmin,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);

 
          if (rank == 0){
            //
            // Computing statistical data
            //
            if (rnavg) {
              absavg +=  rdavg/rnavg;
              nabsavg++;
            }
            if (rdmin < absmin) absmin = rdmin;
          }
        }

        //
        //  move particles
        //
    }
    simulation_time = read_timer( ) - simulation_time;
  
    if (rank == 0) {  
      printf( "n = %d, simulation time = %g seconds", n, simulation_time);

      if( find_option( argc, argv, "-no" ) == -1 )
      {
        if (nabsavg) absavg /= nabsavg;
      // 
      //  -The minimum distance absmin between 2 particles during the run of the simulation
      //  -A Correct simulation will have particles stay at greater than 0.4 (of cutoff) with typical values between .7-.8
      //  -A simulation where particles don't interact correctly will be less than 0.4 (of cutoff) with typical values between .01-.05
      //
      //  -The average distance absavg is ~.95 when most particles are interacting correctly and ~.66 when no particles are interacting
      //
      printf( ", absmin = %lf, absavg = %lf", absmin, absavg);
      if (absmin < 0.4) printf ("\nThe minimum distance is below 0.4 meaning that some particle is not interacting");
      if (absavg < 0.8) printf ("\nThe average distance is below 0.8 meaning that most particles are not interacting");
      }
      printf("\n");     
        
      //  
      // Printing summary data
      //  
      if( fsum)
        fprintf(fsum,"%d %d %g\n",n,n_proc,simulation_time);
    }
  
    //
    //  release resources
    //
    if ( fsum )
        fclose( fsum );
    free( particles );
    if( fsave )
        fclose( fsave );
    
    MPI_Finalize( );
    
    return 0;
}
